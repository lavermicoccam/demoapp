import os

from flask import Flask
from flask import request
from flask import jsonify
from flask_cors import CORS

from pymongo import MongoClient

app = Flask(__name__)
CORS(app)

@app.route("/", methods=["GET"])
def home():
    return jsonify({'result': 'Page working.'}), 200 

@app.route("/login", methods=["POST"])
def login():
    try:
        req = request.get_json()

        db = MongoClient(os.environ['ConnectionString'])['DemoAppDb']
        user = db.users.find_one({'username': req['username']})

        if (req['username'] == user['username'] and req['password'] == user['password']):
            return jsonify({}), 200
        else:
            return jsonify({'result': False}), 401
    except Exception as e:
        print(e.args)

    return jsonify({'result': False}), 401

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8888)