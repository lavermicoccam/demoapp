import axios from 'axios';

console.log(process.env.VUE_APP_APPNAME)

const loginService = axios.create({
  baseURL: 'http://173.249.50.133' + '/login',
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': '*'
  }
})

export default loginService